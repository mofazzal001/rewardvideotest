//
//  ViewController.m
//  RewardVideoTest
//
//  Created by Md Mofazzal Hossain on 6/1/15.
//  Copyright (c) 2015 Md Mofazzal Hossain. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    BOOL isVideoAvailable;
    
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    brandEngageClient = [SponsorPaySDK requestBrandEngageOffersNotifyingDelegate:self];
    
    [SponsorPaySDK requestBrandEngageOffersNotifyingDelegate:self placementId:@"unknown" queryVCSWithCurrencyId:@"USD" vcsDelegate:self];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loadVideoBtnClicked:(id)sender
{
    [brandEngageClient startWithParentViewController:self];
}

- (void)brandEngageClient:(SPBrandEngageClient *)brandEngageClient didChangeStatus:(SPBrandEngageClientStatus)newStatus
{
    if (newStatus == (SPBrandEngageClientStatus)STARTED) {
        NSLog(@"Video is started");
        [self writeToConsole:@"Video is started" toOutputConsole:_console];
    }else if(newStatus == (SPBrandEngageClientStatus)CLOSE_FINISHED){
        NSLog(@"Video is Closed and Finished");
        [self writeToConsole:@"Video is Closed and Finished" toOutputConsole:_console];
    }else if (newStatus == (SPBrandEngageClientStatus)CLOSE_ABORTED){
        NSLog(@"Video is aborted");
        [self writeToConsole:@"Video is Aborted" toOutputConsole:_console];
    }else{
        NSLog(@"Some error occured");
        [self writeToConsole:@"Some error occured" toOutputConsole:_console];
    }
}

- (void)brandEngageClient:(SPBrandEngageClient *)brandEngageClient didReceiveOffers:(BOOL)areOffersAvailable
{
    isVideoAvailable = areOffersAvailable;
}

- (IBAction)checkVideoBtnClicked:(id)sender
{
    if (isVideoAvailable) {
        [self writeToConsole:@"Video is available" toOutputConsole:_console];
    }else{
        [self writeToConsole:@"Video not available yet" toOutputConsole:_console];
    }
}

- (IBAction)clearBtnClicked:(id)sender
{
    _console.text = @"";
}


-(void)virtualCurrencyConnector:(SPVirtualCurrencyServerConnector *)connector didReceiveDeltaOfCoinsResponse:(double)deltaOfCoins currencyId:(NSString *)currencyId currencyName:(NSString *)currencyName latestTransactionId:(NSString *)transactionId
{
    NSLog(@"Received %.2f %@", deltaOfCoins, currencyName);
    [self writeToConsole:[NSString stringWithFormat:@"Received %.2f %@", deltaOfCoins, currencyName] toOutputConsole:_console];
}

-(void)virtualCurrencyConnector:(SPVirtualCurrencyServerConnector *)connector failedWithError:(SPVirtualCurrencyRequestErrorType)error errorCode:(NSString *)errorCode errorMessage:(NSString *)errorMessage
{
    NSLog(@"VCS error received %@", errorMessage);    
    [self writeToConsole:[NSString stringWithFormat:@"Received %@", errorMessage] toOutputConsole:_console];
}


- (void) writeToConsole:(NSString *) writeLog toOutputConsole:(UITextView *)toOutputConsole
{
    NSString *prevLog = toOutputConsole.text;
    writeLog = [writeLog stringByAppendingString:@"\n"];
    if (prevLog != nil) {
        writeLog = [writeLog stringByAppendingString:prevLog];
    }
    toOutputConsole.text = writeLog;
}


@end
