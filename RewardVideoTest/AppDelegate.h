//
//  AppDelegate.h
//  RewardVideoTest
//
//  Created by Md Mofazzal Hossain on 6/1/15.
//  Copyright (c) 2015 Md Mofazzal Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

