//
//  ViewController.h
//  RewardVideoTest
//
//  Created by Md Mofazzal Hossain on 6/1/15.
//  Copyright (c) 2015 Md Mofazzal Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SponsorPaySDK.h"


@interface ViewController : UIViewController<SPBrandEngageClientDelegate, SPVirtualCurrencyConnectionDelegate>
{
    SPBrandEngageClient *brandEngageClient;
}

@property (nonatomic, strong) IBOutlet UITextView *console;

- (IBAction)loadVideoBtnClicked:(id)sender;
- (IBAction)checkVideoBtnClicked:(id)sender;
- (IBAction)clearBtnClicked:(id)sender;

@end

